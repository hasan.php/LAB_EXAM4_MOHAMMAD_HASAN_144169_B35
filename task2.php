<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Modal Example</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link href="assets/css/footer.css" rel="stylesheet">


</head>

<body>

<!-- Begin page content -->
<div class="container">
    <div class="page-header">
        <h1>Example of Modal</h1>
    </div>
    <p class="lead">Click in the button below!</p>

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">
        Click Here!
    </button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Your data</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="carousel.php" enctype="multipart/form-data">
                       <div class="form-group">
                        <label for="fileName">File Name:</label>
                        <input type="text" class="form-control" name="fileName" id="fileName" placeholder="Enter your file name here">
                    </div>
                        <div class="form-group">
                            <label for="Date">Date :</label>
                            <input type="date" class="form-control" name="date" id="date" placeholder="Select a date">
                        </div>
                        <div class="form-group">
                            <fieldset >
                                <input type="file" name="imgFile" id="imgFile">
                                <input type="submit" value="Upload" class="btn btn-info">
                            </fieldset>

                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    Modal Footer
                </div>
            </div>
        </div>
    </div>

</div>

<footer class="footer">
    <div class="container">
        <p class="text-muted">Eftequarul Alam SEID 137008 BITM Batch 33</p>
    </div>
</footer>


</body>
</html>